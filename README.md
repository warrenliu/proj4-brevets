# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young for the initial version of this code.

## Authors
* Init by M Young
* Revised by Yiran Liu

## Use
```
docker build .
```
```
docker run -d -p 5000:5000 <container_name>
```

## Test
* Nose test file is included
